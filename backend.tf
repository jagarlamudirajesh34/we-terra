terraform {
  backend "s3" {
    bucket = "rmule"
    key    = "terraform.tfstate"
    region = "ap-south-1"
  }
}