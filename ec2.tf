provider "aws" {
 profile = "default"
 region = "ap-south-1"
}

resource "aws_instance" "myec2" {
  ami = "ami-02e60be79e78fef21" 
  instance_type = "t2.micro"
  subnet_id = "subnet-de91e092"
  security_groups = ["sg-0ab2f710e90fc404c"] 
  tags = {
     Name = "my-ssm"
   }
  key_name = "rajesh"
}